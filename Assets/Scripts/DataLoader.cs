﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataLoader : MonoBehaviour
{
    public static DataLoader instance = null;

    private int highScore;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        Load();
    }

    public void Save()
    {
        PlayerPrefs.SetInt("HighScore", highScore);
    }

    public void Load()
    {
        PlayerPrefs.GetInt("HighScore", 0);
    }

    public int GetHighScore()
    {
        return highScore;
    }

    public void NewHighScore(int highScore)
    {
        this.highScore = highScore;
    }

    private void OnApplicationQuit()
    {
        Save();
    }
}
