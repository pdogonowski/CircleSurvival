﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : MonoBehaviour
{
    private bool running = false; // When running timer counts and Circles are ticking
    private float startTime = 0f;
    private float lastValue = 0f;

    // Update is called once per frame
    void Update()
    {
        float deltaT = GetTime();

        float minutes = (deltaT / 60);
        float seconds = (deltaT % 60);

        GetComponent<UnityEngine.UI.Text>().text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }

    // Called by first green button on click, connected in inspector
    public void OnStart()
    {
        running = true;
        startTime = Time.unscaledTime;
    }

    public float GetTime()
    {
        if (running)
        {
            lastValue = Time.unscaledTime - startTime;
        }

        return lastValue;
    }

    public bool IsRunning()
    {
        return running;
    }

    public void StopTimer()
    {
        running = false;
    }
}
