﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleGenerator : MonoBehaviour
{
    private float lastCircleSpawntime = 0f;
    private float timeToSpawnNext = 0.5f; // Must be greater than 0f
    private float blackCircleChance = 0.1f;
    private Rect area;
    private float radius = 50;

    [SerializeField]
    private GameObject greenCircle;
    [SerializeField]
    private GameObject blackCircle;

    // Start is called before the first frame update
    void Start()
    {
        area = GetComponent<RectTransform>().rect;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.instance.IsStarted())
        {
            if (TimeFromLastSpawn() >= timeToSpawnNext)
            {
                // Layers to collide in raycasting
                int layerMask = LayerMask.GetMask("Circles");

                float x, y;

                // Random position
                do
                {
                    x = Random.Range(area.xMin, area.xMax);
                    y = Random.Range(area.yMin, area.yMax);
                } while (CenterCollides(new Vector3(x, y, 0)));

                GameObject circle;

                // Spawn green
                if (Random.Range(0f, 1f) >= blackCircleChance)
                {
                    circle = Object.Instantiate(greenCircle, gameObject.transform, false);
                } else // or black
                {
                    circle = Object.Instantiate(blackCircle, gameObject.transform, false);
                }

                circle.transform.localPosition = new Vector3(x, y, 0f);

                float currentTime = GameManager.instance.GetTime();
                timeToSpawnNext = 3 / Mathf.Sqrt(currentTime);
                lastCircleSpawntime = currentTime;
            }
        }
    }

    private bool CenterCollides(Vector3 center)
    {
        bool collides = false;
        foreach (Transform g in gameObject.GetComponentsInChildren<Transform>())
        {
            if (g.tag != "SpawnArea" &&  Vector3.Distance(center, g.localPosition) <= 2 * radius)
            {
                collides = true;
                break;
            }
        }

        return collides;
    }

    private float TimeFromLastSpawn()
    {
        return GameManager.instance.GetTime() - lastCircleSpawntime;
    }
}
