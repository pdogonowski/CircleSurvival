﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndgameUI : MonoBehaviour
{
    private string prefix = "Score: ";

    [SerializeField]
    private UnityEngine.UI.Text resultInfo;

    private void Update()
    {
        int score = GameManager.instance.GetScore();
        resultInfo.text = prefix + score.ToString();
    }

    public void NewHighScore()
    {
        prefix = "High Score :";
    }
}
