﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackCircle : Circle
{
    public override void OnTouch()
    {
        GameManager.instance.OnBlackCircleTapped();
        Destroy(gameObject);
    }

    // Start is called before the first frame update
    new void Start()
    {
        base.Start();
        lifeTime = 3f;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.instance.GetTime() - creationTime >= lifeTime)
        {
            Destroy(gameObject);
        }
    }
}
