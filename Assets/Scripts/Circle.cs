﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class Circle : MonoBehaviour
{
    protected float creationTime;
    protected float lifeTime;

    // Start is called before the first frame update
    protected void Start()
    {
        creationTime = GameManager.instance.GetTime();

        // null returned if component not found
        GetComponent<Button>().onClick.AddListener(OnTouch);
    }

    public abstract void OnTouch();
}
