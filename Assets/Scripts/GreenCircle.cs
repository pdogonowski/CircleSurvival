﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GreenCircle : Circle
{
    [SerializeField]
    private GameObject redCircle;

    public override void OnTouch()
    {
        GameManager.instance.OnGreenCircleTapped();
        Destroy(gameObject);
    }

    // Start is called before the first frame update
    new void Start()
    {
        base.Start();
        lifeTime = Random.Range(2f, 4f);
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.instance.IsStarted())
        {
            if (GameManager.instance.GetTime() - creationTime >= lifeTime)
            {
                GameManager.instance.OnGreenCircleDie();
            }

            float age = GameManager.instance.GetTime() - creationTime;
            redCircle.GetComponent<UnityEngine.UI.Image>().fillAmount = age / lifeTime;
        }
    }
}
