﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Counts current score, detects High Score,
// Opens GameLost(EndGame) menu
public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;

    [SerializeField]
    private EndgameUI endgameUI;

    [SerializeField]
    private Timer timer;

    [SerializeField]
    private int score = 0;
    private int highScore;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    public void OnGreenCircleTapped()
    {
        float points = Mathf.Pow(timer.GetTime(), 1.1f);
        score += (int)points;
    }

    public void OnBlackCircleTapped()
    {
        GameLost();
    }

    public void OnGreenCircleDie()
    {
        GameLost();
    }

    private void GameLost()
    {
        if (score > highScore)
        {
            highScore = score;
            endgameUI.NewHighScore();
            DataLoader.instance.NewHighScore(highScore);
        }

        // Pause game
        timer.StopTimer();

        // Show result UI
        endgameUI.gameObject.SetActive(true);
    }

    // Zapisywanie i wczytywanie wyniku
    public int GetScore()
    {
        return score;
    }

    void Start()
    {
        endgameUI.gameObject.SetActive(false);
        score = 0;
        highScore = DataLoader.instance.GetHighScore();
    }

    public bool IsStarted()
    {
        return timer.IsRunning();
    }

    public float GetTime()
    {
        return timer.GetTime();
    }
}
