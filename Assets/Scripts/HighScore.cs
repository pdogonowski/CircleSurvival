﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HighScore : MonoBehaviour
{
    [SerializeField]
    private UnityEngine.UI.Text text;

    // Start is called before the first frame update
    void Start()
    {
        int score = DataLoader.instance.GetHighScore();
        text.text = "High Score :" + score.ToString();
    }
}
